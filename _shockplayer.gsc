// Shellshock add-on by kel, dla serwera FAMILKA.
// Efekt og�uszenia na wybranego gracza.
// Instalacja:
// 1) Plik _shockplayer.gsc wrzuci� do folderu gametypes.
// 2) W pliku tdm.gsc we funkcji Callback_StartGameType() dopisa� linijk�: thread maps\mp\gametypes\_shockplayer::shockplayer();
// 3) Zapisa� zmiany i cieszy� si� now� funkcj�.
// Wywo�anie funkcji poprzez komend�: /rcon shock nrguid
// Pozdrawiam
// kel@familka.info
// www.familka.info
shockplayer()
{
	level endon("boot");

	setcvar("shock", "");
	while(1)
	{
		if(getcvar("shock") != "" && getcvar("shock") != "all")
		{
			killPlayerNum = getcvarint("shock");
			players = getentarray("player", "classname");
			for(i = 0; i < players.size; i++)
			{
				thisPlayerNum = players[i] getEntityNumber();
				if(thisPlayerNum == killPlayerNum && isAlive(players[i]) ) 
				{
					players[i] shellshock("default", 15);
					players[i] iprintlnbold("^1Zosta�e� og�uszony przez admina!!!");
					iprintln(players[i].name + "^7 zostal ogluszony przez admina.");
				}
			}
			setcvar("shock", "");
		}
		else
		if(getcvar("shock") == "all")
		{			
			players = getentarray("player", "classname");
			for(i = 0; i < players.size; i++)
			{				
				if( isAlive(players[i]) ) 
				{
					players[i] shellshock("default", 15);
					players[i] iprintlnbold("^1Zosta�e� og�uszony przez admina!!!");
					iprintln(players[i].name + "^7 zostal ogluszony przez admina.");
				}
			}
			setcvar("shock", "");
		}
		wait 0.05;
	}
}