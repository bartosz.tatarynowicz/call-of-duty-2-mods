// -------------------------
// kelmods.gsc (c) 2012-2013 kel
// Plik ze wszystkimi modyfikacjami dla serwera FAMILKA.
// -------------------------
// Spis aktualnych mod�w i funkcji:
//  - losowa bro�
//  - grenade mode
//  - shotgun mode
//  - panzerschreck mode
//  - force talk mode
//  - zapisywanie przebiegni�tych metr�w do log�w
// -------------------------
// kontakt:
// www.familka.info
// kel@familka.info
// gg: 3178519


// Callback_StartGameType w tdm.gsc
main()
{
	// p�ki co nic
	thread levelMonitor();
}
// Callback_PlayerConnect() w tdm.gsc
Callback_PlayerConnect()
{
	//modMonitor
	thread modMonitor();
}
// wywo�ywane na self przy disconnect
selfOnDisconnect()
{
	
	lpselfnum = self getEntityNumber();
	lpGuid = self getGuid();	
	// zapis metra�u do log�w
	if(self.przebiegniete_metry != 0) logPrint("FOOTDIST;"+ lpGuid + ";" + lpselfnum + ";" + self.name + ";" + self.przebiegniete_metry + "\n");
	//killstreak self.maxkillstreak
	if(self.maxkillstreak != 0) logPrint("MAXKILLSTREAK;"+ lpGuid + ";" + lpselfnum + ";" + self.name + ";" + self.maxkillstreak + "\n");
}
// wywo�ywane na level przy endMap
endMap()
{
	//zapisz wszystkim metra�
	players = getentarray("player", "classname");
	for(i = 0; i < players.size; i++)
	{
		player = players[i];
		lpselfnum = player getEntityNumber();
		lpGuid = player getGuid();
		if(self.przebiegniete_metry != 0) logPrint("FOOTDIST;"+ lpGuid + ";" + lpselfnum + ";" + player.name + ";" + player.przebiegniete_metry + "\n");
		if(self.maxkillstreak != 0) logPrint("MAXKILLSTREAK;"+ lpGuid + ";" + lpselfnum + ";" + self.name + ";" + self.maxkillstreak + "\n");
	}
}
// spawnPlayer() w tdm.gsc
spawnPlayer()
{
	self.czyZabranoGranaty = 0;
	self.czyZabranoPompa = 0;
	self.czyZabranoSzrek = 0;
}
// p�tla globalna, dla calego levelu (mapy)
levelMonitor()
{
	level endon("boot");
	setcvar("niechpowie", "");
	while(1)
	{
		thread forceTalk();
		wait 0.2;
	}
}
// forceTalk
forceTalk()
{
	zmienna = getCvar("niechpowie");
	if(zmienna != "")
	{
		// czas zmusi� go do m�wienia
		// /rcon set bm 1 abcd
		// albo slot 0-9 (wtedy substring zawiera spacje)
		slot = getSubStr(zmienna, 0, 2); //"bob"
		tresc = getSubStr(zmienna, 2);
		if(isSubStr(slot, " ")) // je�li spacja jest w slocie to numerek jest z przedzia�u 0-9
		{
			slot = getSubStr(zmienna, 0, 1); // teraz nie ma tam spacji
		}
		intSlot = Int(slot); // zamie�my na inta w razie czego
		
		// pobierz tablice graczy i znajd� tego, kt�ry ma to powiedzie�
		players = getentarray("player", "classname");
		for(i = 0; i < players.size; i++)
		{
			thisPlayerNum = players[i] getEntityNumber();
			if(thisPlayerNum == intSlot) 
			{
				players[i] sayall(tresc);
			}
		}
		setcvar("niechpowie", "");
	}
}
// modMonitor, czyli funkcja sprawdzaj�ca czy w��czy�/wy��czy� ju� mod
// p�tla dla ka�dego gracza z osobna
modMonitor()
{
	self endon("disconnect");
	while(1)
	{
		// grenade mode
		if(getCvar("grenademode") == "1" && self.czyZabranoGranaty == 0)
		{
			self thread gmZabierzBron();
			self iprintlnbold("^1GRENADE MODE: ON");
			self thread czyDoscGranatow();
		}
		if(getCvar("grenademode") == "0" && self.czyZabranoGranaty == 1)
		{
			self thread gmOddajBron();
			self iprintlnbold("^1GRENADE MODE: OFF");
			level notify("grenademode_off");
		}
		// pompa mode
		if(getCvar("pompamode") == "1" && self.czyZabranoPompa == 0)
		{
			self thread pmZabierzBron();
			self iprintlnbold("^1POMPA MODE: ON");
			//self thread czyDoscGranatow();
		}
		if(getCvar("pompamode") == "0" && self.czyZabranoPompa == 1)
		{
			self thread pmOddajBron();
			self thread maps\mp\gametypes\tdm::respawn();
			self iprintlnbold("^1POMPA MODE: OFF");
			level notify("pompamode_off");
		}
		// szrek mode
		if(getCvar("szrekmode") == "1" && self.czyZabranoSzrek == 0)
		{
			self thread smZabierzBron();
			self iprintlnbold("^1PANZERSCHRECK MODE: ON");
			//self thread czyDoscGranatow();
		}
		if(getCvar("szrekmode") == "0" && self.czyZabranoSzrek == 1)
		{
			self thread smOddajBron();
			self thread maps\mp\gametypes\tdm::respawn();
			self iprintlnbold("^1PANZERSCHRECK MODE: OFF");
			level notify("szrekmode_off");
		}		
		wait 0.2;
	}
}
// domy�lne oddawanie bronii
oddajBron()
{
	self takeallweapons();
	maps\mp\gametypes\_weapons::givePistol();
	maps\mp\gametypes\_weapons::giveGrenades();
	maps\mp\gametypes\_weapons::giveBinoculars();	
	self giveWeapon(self.pers["weapon"]);
	self giveMaxAmmo(self.pers["weapon"]);
	self setSpawnWeapon(self.pers["weapon"]);
}
// Granade Mod
gmZabierzBron()
{	
	self takeallweapons();	
	maps\mp\gametypes\_weapons::giveGrenades();
	self.czyZabranoGranaty = 1;
}
gmOddajBron()
{	
	self oddajBron();
	self.czyZabranoGranaty = 0;
}
czyDoscGranatow()
{
	level endon("grenademode_off");
	while(1)
	{
		// daj mu amo w gana (granta)!
		maps\mp\gametypes\_weapons::giveGrenades();
		// tutaj manipuluj czasem, 1 oznacza jedn� sekund�, jak chcesz p� sekundy to ustaw 0.5 (z kropk�)
		wait 2;
	}

}
// Pompa Mod
pmZabierzBron()
{
	self takeallweapons();	
	self setWeaponSlotWeapon("primaryb", "shotgun_mp");
	self giveMaxAmmo("shotgun_mp");
	self.czyZabranoPompa = 1;
}
pmOddajBron()
{	
	self oddajBron();
	self.czyZabranoPompa = 0;
}

// Szrek Mod
smZabierzBron()
{
	self takeallweapons();	
	self setWeaponSlotWeapon("primaryb", "panzerschreck_mp");
	self giveMaxAmmo("panzerschreck_mp");
	self.czyZabranoSzrek = 1;
}
smOddajBron()
{	
	self oddajBron();
	self.czyZabranoSzrek = 0;
}

// Random Weapon Mod 
// Funkcja losuj�ca bro� dla gracza.
rwmLosuj()
{
	weaponnames = [];
	weaponnames[0] = "greasegun_mp";
	weaponnames[1] = "m1carbine_mp";
	weaponnames[2] = "m1garand_mp";
	weaponnames[3] = "springfield_mp";
	weaponnames[4] = "thompson_mp";
	weaponnames[5] = "bar_mp";
	weaponnames[6] = "sten_mp";
	weaponnames[7] = "enfield_mp";
	weaponnames[8] = "enfield_scope_mp";
	weaponnames[9] = "bren_mp";
	weaponnames[10] = "PPS42_mp";
	weaponnames[11] = "mosin_nagant_mp";
	weaponnames[12] = "SVT40_mp";
	weaponnames[13] = "mosin_nagant_sniper_mp";
	weaponnames[14] = "ppsh_mp";
	weaponnames[15] = "mp40_mp";
	weaponnames[16] = "kar98k_mp";
	weaponnames[17] = "g43_mp";
	weaponnames[18] = "kar98k_sniper_mp";
	weaponnames[19] = "mp44_mp";
	weaponnames[20] = "kar98k_mp";
	weaponnames[21] = "shotgun_mp";
	indeks = randomIntRange(0,weaponnames.size-1);
	iprintlnbold(indeks);
	return weaponnames[indeks];
}