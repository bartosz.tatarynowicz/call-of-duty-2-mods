// Take Ammo Mod
// Copyright 2012 kel dla serwera FAMILKA
// ####
// Mod pozwalający zabrać konkretnemu graczowi amunicję.
// Po ponownym odrodzeniu amunicja zostaje przywrócona.
// ####
// Kontakt:
// www.familka.info
// kel@familka.info
// gg: 3178519

// wywoływanie przy łączeniu gracza z serwerem
wyzeruj()
{
	self.czyZabrac = 0;
}
takeammo()
{
	self endon("disconnect");
	while(1)
	{			
		hisId = self getEntityNumber();
		if(hisId == getCvarInt("zabierz") || self.czyZabrac == 1)
		{ 		
			// zabierz mu ammo z gana!
			if (self getWeaponSlotWeapon("primary") != "none")
			{
				self setWeaponSlotAmmo("primary", 0);
				self setWeaponSlotClipAmmo("primary", 0);
			}
			if (self getWeaponSlotWeapon("primaryb") != "none")
			{
				self setWeaponSlotAmmo("primaryb", 0);
				self setWeaponSlotClipAmmo("primaryb", 0);
			}

			self takeWeapon("frag_grenade_american_mp");
			self takeWeapon("frag_grenade_british_mp");
			self takeWeapon("frag_grenade_russian_mp");
			self takeWeapon("frag_grenade_german_mp");
			self takeWeapon("smoke_grenade_american_mp");
			self takeWeapon("smoke_grenade_british_mp");
			self takeWeapon("smoke_grenade_russian_mp");
			self takeWeapon("smoke_grenade_german_mp");
			
			// powiadom gracza
			
			if(self.czyZabrac == 0) self iprintlnbold("Twoja amunicja zostala zabrana przez admina!");
			self.czyZabrac = 1;
			// zresetuj cvary
			resetCvars();    			
		}						
		wait 1;
	}
}
resetCvars()
{
	setCvar("zabierz","-1");					
}