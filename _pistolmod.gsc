// #####
// Pistol Mode 0.1 dla serwera Familka
// Autor: kel@familka.info
// #####
// Instalacja:
// 1. Do folderu gametypes wrzuci� plik _pistolmod.gsc
// 2. Otworzy� plik tdm.gsc
// 3. Do funkcji Callback_PlayerConnect() dopisa� linijk�: self thread maps\mp\gametypes\_pistolmod::pistolModMonitor();
// 4. Do funkcji sprawnPlayer() dopisa� linijk�: self.czyZabrano = 0; 
// 5. Do pliku konfiguracyjnego serwera (np. serwer.cfg) dopisa� linijk�: set pistolmode "0"
// 6. Zamkn�� plik tdm.gsc i cieszy� si� now� funkcj�.
// #####
// W��czenie Pistol Mode komend�: /rcon pistolmode 1
// Wy��czenie Pistol Mode komend�: /rcon pistolmode 0
// #####
// Pozdrawiam
// kel@familka.info
// #####

pistolModMonitor()
{
	self endon("disconnect");
	while(1)
	{
		if(getCvar("pistolmode") == "1" && self.czyZabrano == 0)
		{
			self thread zabierzBron();
			self iprintlnbold("^1PISTOL MODE: ON");
			self thread czyNapewnoMaPistolet();
		}
		if(getCvar("pistolmode") == "0" && self.czyZabrano == 1)
		{
			self thread oddajBron();
			self iprintlnbold("^1PISTOL MODE: OFF");
			level notify("pistolmode_off");
		}
		wait 0.2;
	}
}
zabierzBron()
{	
	self takeallweapons();	
	maps\mp\gametypes\_weapons::givePistol();
	self.czyZabrano = 1;
}
oddajBron()
{	
	self takeallweapons();
	maps\mp\gametypes\_weapons::givePistol();
	maps\mp\gametypes\_weapons::giveGrenades();
	maps\mp\gametypes\_weapons::giveBinoculars();	
	self giveWeapon(self.pers["weapon"]);
	self giveMaxAmmo(self.pers["weapon"]);
	self setSpawnWeapon(self.pers["weapon"]);
	self.czyZabrano = 0;
}
czyNapewnoMaPistolet()
{
	level endon("pistolmode_off");
	while(1)
	{
		//sprawd�, czy jego bron to na pewno pistolet
		weapon3 = self getweaponslotweapon("primaryb");
		weapon4 = self getweaponslotweapon("primary");
		if(maps\mp\gametypes\_weapons::isPistol(weapon3) && !(maps\mp\gametypes\_weapons::isMainWeapon(weapon4)))
		{
			// bardzo dobrze!
		}
		else
		{
			// nie dobrze! zabra� mu!
			self thread zabierzBron();
		}
		wait 0.2;
	}

}