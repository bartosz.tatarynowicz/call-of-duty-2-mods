// Meters Counter + Kill Distance Counter
// Copyright 2012 kel dla serwera FAMILKA
// ####
// Mod zliczaj�cy i wy�wietlaj�cy przebyt� odleg�o��.
// +
// Mod wy�wietlaj�cy odleg�o�� z jakiej zabito gracza.
// ####
// Data utworzenia: 17.08.2012.
// ####
// Kontakt:
// www.familka.info
// kel@familka.info
// gg: 3178519

// wyzerowanie licznika
wyzeruj()
{
	self.przebiegniete = 0;
	self.przebiegniete_metry = 0;
}

// zliczanie odleg�o�ci
zliczajOdleglosc()
{
	self endon("disconnect");
	self endon("joined_spectators");
	self endon("killed_player");
	self waittill("spawned_player");
	
	while(1)
	{			
		oldorigin = self.origin;
		wait 1;
		neworigin = self.origin;
		self.przebiegniete = self.przebiegniete + distance(oldorigin, neworigin);
		self.przebiegniete_metry = Int(self.przebiegniete * 0.0254);
		//iprintln(self.przebiegniete);
		self updateKillstreakHUD();		
	}
}

// wy�wietlanie przebytej odleglosci
// precache tekstu
distancePrecache()
{
	precacheString(&"Przebyty dystans: &&1 m.");
	precacheString(&"Zabity z odleglosci: &&1 m.");
	level thread onPlayerConnect();
}
// funkcje na playerze gdy si� ��czy
onPlayerConnect()
{
	for(;;)
	{
		level waittill("connecting", player);
		player thread onPlayerSpawned();
		player thread onJoinedTeam();
		player thread onJoinedSpectators();
		player thread onUpdatePlayerScoreHUD();
		player thread wyzeruj();
		
	}
}
// funkcje gdy gracz do��czy do dru�yny
onJoinedTeam()
{
	self endon("disconnect");
	for(;;)
	{
		self waittill("joined_team");
		self thread removeKillstreakHUD();
		self thread zliczajOdleglosc();		
	}
}
// funkcje gdy gracz przejdzie na specta
onJoinedSpectators()
{
	self endon("disconnect");
	for(;;)
	{
		self waittill("joined_spectators");
		self thread removeKillstreakHUD();
	}
}
// funkcje gdy gracz si� zespawnuje
onPlayerSpawned()
{
	self endon("disconnect");
	
	for(;;)
	{	
		self waittill("spawned_player");
		
		// wy�wietlanie dystansu
		if(!isdefined(self.distancehud) && self.show_ks)
		{
			self.distancehud = newClientHudElem(self);
			self.distancehud.horzAlign = "middle";
			self.distancehud.vertAlign = "bottom";
			self.distancehud.alignX = "left";
			self.distancehud.alignY = "middle";
			self.distancehud.x = 2;
			self.distancehud.y = -10;
			self.distancehud.archived = false;
			self.distancehud.font = "default";
			self.distancehud.fontscale = 1;
			self.distancehud.label = &"Przebyty dystans: &&1 m.";
		}
		self thread updateKillstreakHUD();
		
	}
}
// przy updejcie score (pozosta�o�� z killstreak�w)
onUpdatePlayerScoreHUD()
{
	for(;;)
	{
		self waittill("update_playerscore_hud");

		self thread updateKillstreakHUD();
	}
}
// od�wie�enie wy�wietlacza
updateKillstreakHUD()
{
	if(isDefined(self.distancehud)) self.distancehud setValue(self.przebiegniete_metry);	
}
// usuni�cie wy�wietlacza
removeKillstreakHUD()
{
	if(isDefined(self.distancehud)) self.distancehud destroy();
}

// ODLEG�O�� PRZY ZABICIU

init()
{
	self.dystans = 0;
}
showdist()
{
	// poka� odleg�o�� na ekranie
	self wyswietlOdleglosc();
	// poczekaj chwile
	wait 4;
	// schowaj odleg�o��
	self removeHUD();
}
killdistPrecache()
{
	precacheString(&"Gracz zabity z odleglosci: &&1 m.");
}
wyswietlOdleglosc()
{
		// wy�wietlanie dystansu
		if(!isdefined(self.killdisthud) && self.show_ks)
		{
			self.killdisthud = newClientHudElem(self);
			self.killdisthud.horzAlign = "center_safearea";
			self.killdisthud.vertAlign = "center_safearea";
			self.killdisthud.alignX = "center";
			self.killdisthud.alignY = "middle";
			self.killdisthud.x = 0;
			self.killdisthud.y = 180;
			self.killdisthud.archived = false;
			self.killdisthud.font = "default";
			self.killdisthud.fontscale = 1;
			self.killdisthud.label = &"Gracz zabity z odleglosci: &&1 m.";
		}
		self thread updateHUD();
		
}
updateHUD()
{
	if(isDefined(self.killdisthud)) self.killdisthud setValue(self.dystans);	
}
removeHUD()
{
	if(isDefined(self.killdisthud)) self.killdisthud destroy();
}